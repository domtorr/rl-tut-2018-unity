﻿using System;
using System.Collections;
using System.Collections.Generic;
using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class MapCreationSystem : ComponentSystem {
    private struct MapGroup
    {
        public TileComponent TileComponent;
        public MapComponent MapComponent;
    }
    
    private struct PlayerGroup
    {
        public InputComponent InputComponent;
        public Transform Transform;
        public MoveComponent MoveComponent;
    }

    protected void ClearMap(Tilemap floors, Tilemap walls)
    {
        floors.ClearAllTiles();
        walls.ClearAllTiles();
    }

    protected MapTile[,] InitMapArray(Tilemap floorMap, Tilemap wallMap, int mapWidth, int mapHeight)
    {        
        ClearMap(floorMap, wallMap);
        
        
        var mapTiles = new MapTile[mapWidth, mapHeight];
        

        for (int x = 0; x < mapTiles.GetUpperBound(0); x++)
        {
            for (int y = 0; y < mapTiles.GetUpperBound(1); y++)
            {
                mapTiles[x, y].Blocked = true;
                mapTiles[x, y].BlockSight = true;
            }
        }

        return mapTiles;
    }

    protected void CreateDungeon(ref MapTile[,] mapTiles, int mapWidth,
        int mapHeight, int minRoomSize, int maxRoomSize, int maxRooms,
        Transform playerPosition)
    {
        var rooms = new List<RoomParams>();
        var numRooms = 0;

        for (int r = 0; r < maxRooms; r++)
        {
            var w = Random.Range(minRoomSize, maxRoomSize);
            var h = Random.Range(minRoomSize, maxRoomSize);
            var x = Random.Range(0, mapWidth - w - 1);
            var y = Random.Range(0, mapHeight - h - 1);
            
            var newRoom = new RoomParams(x, y, w, h);
            bool failed = false;
            foreach (var room in rooms)
            {
                if (DoesIntersect(newRoom, room))
                {
                    failed = true;
                    break;
                }
            }

            if (!failed)
            {
                CreateRoom(ref mapTiles, newRoom);

                var newX = newRoom.centerX;
                var newY = newRoom.centerY;

                if (rooms.Count == 0)
                {
                    playerPosition.transform.position = new Vector3(newX, newY, -1);
                }
                else
                {
                    var prevX = rooms[rooms.Count - 1].centerX;
                    var prevY = rooms[rooms.Count - 1].centerY ;
                    if (Random.Range(0, 2) == 1)
                    {
                        CreateHorizontalTunnel(ref mapTiles, prevX, newX, prevY);
                        CreateVerticalTunnel(ref mapTiles, prevY, newY, newX);
                    }
                    else
                    {
                        CreateVerticalTunnel(ref mapTiles, prevY, newY, prevX);
                        CreateHorizontalTunnel(ref mapTiles, prevX, newX, newY);
                    }
                }
                
                rooms.Add(newRoom);
            }
        }
    }


    protected bool DoesIntersect(RoomParams room1, RoomParams room2)
    {
        return (room1.x1 <= room2.x2 && room1.x2 >= room2.x1 &&
                room1.y1 <= room2.y2 && room1.y2 >= room2.y1);
    }

    protected void CreateRoom(ref MapTile[,] mapTiles, RoomParams roomParams)
    {
        for (int x = roomParams.x1 + 1; x < roomParams.x2; x++)
        {
            for (int y = roomParams.y1 + 1; y < roomParams.y2; y++)
            {
                mapTiles[x, y].Blocked = false;
                mapTiles[x, y].BlockSight = false;
            }
        }
    }

    protected void CreateHorizontalTunnel(ref MapTile[,] mapTiles, int x1, int x2, int y)
    {
        for (int x = math.min(x1, x2); x <= math.max(x1, x2); x++)
        {
            mapTiles[x, y].Blocked = false;
            mapTiles[x, y].BlockSight = false;
        }
    }

    protected void CreateVerticalTunnel(ref MapTile[,] mapTiles, int y1, int y2, int x)
    {
        for (int y = math.min(y1, y2); y <= math.max(y1, y2); y++)
        {
            mapTiles[x, y].Blocked = false;
            mapTiles[x, y].BlockSight = false;
        }
    }


    protected override void OnStartRunning()
    {
        var player = GetEntities<PlayerGroup>()[0];
        var playerTransform = player.Transform;
        
        foreach (var e in GetEntities<MapGroup>())
        {
            var mapTiles = new MapTile[e.MapComponent.Width,e.MapComponent.Height];
            if (!e.MapComponent.MapGenerated)
            {
                mapTiles = InitMapArray(
                    e.MapComponent.FloorsMap, e.MapComponent.WallsMap,
                    e.MapComponent.Width, e.MapComponent.Height);
                
                CreateDungeon(ref mapTiles, e.MapComponent.Width,
                    e.MapComponent.Height, e.MapComponent.MinRoomsize,
                    e.MapComponent.MaxRoomSize, e.MapComponent.MaxRooms, playerTransform);
            }

            player.MoveComponent.newPosition = player.Transform.position;
            e.TileComponent.MapTiles = mapTiles;
            e.MapComponent.MapGenerated = true;
        }
    }


    protected override void OnUpdate()
    {
    }
}
