﻿using System;
using System.Collections;
using System.Collections.Generic;
using Components;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class PlayerMovementSystem : ComponentSystem {

    private struct MoveGroup
    {
        public Transform Transform;
        public InputComponent InputComponent;
        public MoveComponent MoveComponent;
    }

    private struct MapGroup
    {
        public ComponentArray<TileComponent> Tiles;
        public int Length;
    }

    [Inject] private MapGroup _map;
    protected override void OnUpdate()
    {
        var dt = Time.deltaTime;
        foreach (var e in GetEntities<MoveGroup>())
        {
            var up = e.InputComponent.Up;
            var down = e.InputComponent.Down;
            var left = e.InputComponent.Left;
            var right = e.InputComponent.Right;
            var transPos = e.Transform.position;
            var newPos = e.MoveComponent.newPosition;
            var speed = e.MoveComponent.Speed;

            var mapTiles = _map.Tiles[0].MapTiles;
            var currentX = Mathf.FloorToInt(e.Transform.position.x);
            var currentY = Mathf.FloorToInt(e.Transform.position.y);
            if (up && transPos == newPos)
            {
                var newUpY = Mathf.FloorToInt((e.MoveComponent.newPosition + Vector3.up).y);
                if (!mapTiles[currentX, newUpY].Blocked)
                {
                    e.MoveComponent.newPosition += Vector3.up;
                }
                e.InputComponent.Up = false;
            }
            else if (down && transPos == newPos)
            {
                var newDownY = Mathf.FloorToInt((e.MoveComponent.newPosition + Vector3.down).y);
                if (!mapTiles[currentX, newDownY].Blocked)
                {
                    e.MoveComponent.newPosition += Vector3.down;
                }
                e.InputComponent.Down = false;
            }
            else if (left && transPos == newPos)
            {
                var newLeftX = Mathf.FloorToInt((e.MoveComponent.newPosition + Vector3.left).x);
                if (!mapTiles[newLeftX, currentY].Blocked)
                {
                    e.MoveComponent.newPosition += Vector3.left;
                }
                e.InputComponent.Left = false;
            }
            else if (right && transPos == newPos)
            {
                var newRightX = Mathf.FloorToInt((e.MoveComponent.newPosition + Vector3.right).x);
                if (!mapTiles[newRightX, currentY].Blocked)
                {
                    e.MoveComponent.newPosition += Vector3.right;
                }
                e.InputComponent.Right = false;
            }
            
            e.Transform.position = Vector3.MoveTowards(transPos, newPos, speed * dt);
        }
    }
}
