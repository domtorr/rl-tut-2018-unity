﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class CameraMovementSystem : ComponentSystem {
	private struct Group
	{
		public CameraComponent CameraComponent;
		public Transform Transform;
	}
	
	protected override void OnUpdate()
	{
		var playerTransform = GameObject.Find("Player").GetComponent<Transform>();
		var playerPosX = playerTransform.position.x;
		var playerPosY = playerTransform.position.y;

		foreach (var e in GetEntities<Group>())
		{
			if (e.CameraComponent.MoveWithPlayer)
			{
				e.Transform.position = new Vector3(playerPosX, playerPosY, e.CameraComponent.TransformZ);
			}
		}
	}
}
