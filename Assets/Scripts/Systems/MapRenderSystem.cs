﻿using System.Collections;
using System.Collections.Generic;
using Components;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapRenderSystem : ComponentSystem {
    private struct MapGroup
    {
        public TileComponent TileComponent;
        public MapComponent MapComponent;
    }
    
    protected void RenderMap(ref MapTile[,] mapTiles, Tilemap floorMap,
        Tilemap wallMap, Tilemap shadowMap, Tilemap fogMap, TileBase floorTile,
        TileBase wallTile, TileBase shadowTile)
    {
        for (int x = 0; x < mapTiles.GetUpperBound(0); x++)
        {
            for (int y = 0; y < mapTiles.GetUpperBound(1); y++)
            {
                if (mapTiles[x, y].Blocked)
                {
                    wallMap.SetTile(new Vector3Int(x, y, 0), wallTile);
                }
                else
                {
                    floorMap.SetTile(new Vector3Int(x, y, 0), floorTile);
                }

                if (mapTiles[x, y].Visible)
                {
                    shadowMap.SetTile(new Vector3Int(x, y, 0), null);
                    fogMap.SetTile(new Vector3Int(x, y, 0), null);
                }

                if (!mapTiles[x, y].Visible && !mapTiles[x, y].hasBeenSen)
                {
                    shadowMap.SetTile(new Vector3Int(x, y, 0), shadowTile);
                    fogMap.SetTile(new Vector3Int(x, y, 0), shadowTile);
                    fogMap.SetColor(new Vector3Int(x, y, 0), new Color(1, 1, 1, 0.5f));
                }

                if (!mapTiles[x, y].Visible && mapTiles[x, y].hasBeenSen)
                {
                    fogMap.SetTile(new Vector3Int(x, y, 0), shadowTile);
                    fogMap.SetColor(new Vector3Int(x, y, 0), new Color(1, 1, 1, 0.5f));
                }
            }
        }   
    }
    
    protected override void OnUpdate()
    {
        foreach (var entity in GetEntities<MapGroup>())
        {
            var mapTiles = entity.TileComponent.MapTiles;
            var floorMap = entity.MapComponent.FloorsMap;
            var wallMap = entity.MapComponent.WallsMap;
            var shadowMap = entity.MapComponent.ShadowMap;
            var fogMap = entity.MapComponent.FogMap;
            var floorTile = entity.TileComponent.FloorTile;
            var wallTile = entity.TileComponent.WallTile;
            var shadowTile = entity.TileComponent.ShadowTile;
            
            RenderMap(ref mapTiles, floorMap, wallMap, shadowMap, fogMap,
                        floorTile, wallTile, shadowTile);
        }
        
    }
}
