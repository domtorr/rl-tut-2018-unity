﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Components
{
    public class MapComponent : MonoBehaviour
    {
        public bool MapGenerated = false;
        public int Width = 80;
        public int Height = 50;
        public int MinRoomsize = 6;
        public int MaxRoomSize = 10;
        public int MaxRooms = 30;
        public Tilemap FloorsMap;
        public Tilemap WallsMap;
        public Tilemap ShadowMap;
        public Tilemap FogMap;
    }
}