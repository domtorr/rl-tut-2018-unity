﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Components
{
    public struct MapTile
    {
        public bool BlockSight;
        public bool Blocked;
        public bool Visible;
        public bool hasBeenSen;
    }

    public class TileComponent : MonoBehaviour
    {
        public TileBase FloorTile;
        public TileBase WallTile;
        public TileBase ShadowTile;
        public Color Color;
        public MapTile[,] MapTiles;
    }
}