﻿using System.Collections;
using System.Collections.Generic;
using Unity.Rendering;
using UnityEngine;

public class InputComponent : MonoBehaviour
{
    public bool Up = false;
    public bool Down = false;
    public bool Left = false;
    public bool Right = false;
}
