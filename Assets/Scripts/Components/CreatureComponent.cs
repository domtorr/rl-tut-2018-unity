﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureComponent : MonoBehaviour
{
    public Color Color;
    public bool isPlayer;
    public bool isHostile;
}
