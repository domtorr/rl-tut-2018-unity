﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RoomParams
{
    public int x1;
    public int y1;
    public int w;
    public int h;
    public int x2;
    public int y2;
    public int centerX;
    public int centerY;

    public RoomParams(int x1, int y1, int w, int h)
    {
        this.x1 = x1;
        this.y1 = y1;
        this.w = w;
        this.h = h;
        this.x2 = x1 + w;
        this.y2 = y1 + h;
        this.centerX = (int) ((this.x1 + this.x2) / 2);
        this.centerY = (int) ((this.y1 + this.y2) / 2);
    }
}

public class RoomComponent : MonoBehaviour
{
    public RoomParams RoomParams;
}
