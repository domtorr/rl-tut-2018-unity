﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComponent : MonoBehaviour
{
    public bool MoveWithPlayer = true;
    public float TransformZ = -10.0f;
}
